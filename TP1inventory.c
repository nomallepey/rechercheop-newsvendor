#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include "TP1inventoryFunctions.h"
#include<ilcplex/cplex.h>


int main(int argc, char **argv)
{
	int rval =0;	
	//File instance name
	//-f option
	char instance_file[1024];
	double cost=0.5;
	double p=1;
	double s=0.3;
	snprintf(       instance_file,
			1024,
			"%s",
			"instanceTest");
	char c;
	while ((c=getopt (argc, argv,"f:c:p:s:h")) != EOF)
	{
		switch(c)
		{
			case 'f':
				snprintf(       instance_file,
						1024,
						"%s",
						optarg);
				break;

			case 'c':
				cost = atof(optarg);
				break;
			case 'p':
				p = atof(optarg);
				break;
			case 's':
				s = atof(optarg);
				break;

			case 'h':
				fprintf(stderr,"Usage: ./newsvendor [options]\nOptions:\n\n");
				fprintf(stderr,"******** INSTANCE DATA ********\n");
				fprintf(stderr,
						"\t-f Instance file name to load..................(default %s).\n",instance_file);
				fprintf(stderr,
						"\t-c production cost.............................(default %lf).\n",cost);
				fprintf(stderr,
						"\t-s Storage cost................................(default %lf).\n",s);
				fprintf(stderr,
						"\t-p sale price..................................(default %lf).\n",p);

				exit(0);
				break;
			default:
				exit(0);
		}
	}


	dataSet data;
	data.p = p;
	data.c = cost;
	data.s = s;

	//Open the instance file
	FILE* fin = fopen(instance_file,"r");
	read_newsvendor_samples(fin,&data);
	fclose(fin);

	//displaying the instance
	display_instance(&data);

	//plotting exponential objective
	plot_objective_exponential(&data);


	//empirical distribution computation
	empirical_distribution(&data);

	//qstar computation
	find_optimal_q(&data);

	//density computation
	compute_density(&data);

	//plotting objective value in function of q
	plot_objective_function(&data);

	free(data.d_k);

	return rval;
}


