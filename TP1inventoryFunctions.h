#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <errno.h>
#include <time.h>
#include<ilcplex/cplex.h>
#include <assert.h>

//IP problem structure
typedef struct IP_problem
{
	//Internal structures
	CPXENVptr env;
	CPXLPptr lp;
	//number of variables
	int nv;
	//number of constraints
	int nc;

	//Output solution
	double *x;
	//Costs array
	double *cost;
	//TYpe of the variables (binary or continuous)
	char *c_type;
	//Bounds over the variables
	double *up_bound;
	double *low_bound;
	//Names of the variables
	char** var_name;

	//Right hand section of the constraints
	double *rhs;
	//Sense of the constraints
	char *sense;
	//Left hand section for data constraints
	int *rmatbeg;
	int *rmatind;
	double *rmatval;
	int nz;
	//Names of the constraints 
	char** const_name;
	//Solution status
	int solstat;
	double objval;
} IP_problem;

typedef struct dataSet 
{
	//Attributes of the instance
	int seed;
	
	//Number of samples
	int K;
	//Samples
	double*d_k;

	//production cost
	double c;
	//storage cost
	double s;
	//sale price
	double p;

	//Number of different values for the cumulative distribution
	int support_size;
	//Support values
	double*values;
	//cumulated distribution
	double*acc;
	//density 
	double* density;

	//optimal q
	double qstar;

} dataSet;

int display_instance(dataSet* dsptr);
int read_newsvendor_samples(FILE*fin,dataSet* dsptr);
int free_IPproblem(IP_problem* ipprobptr);


int plot_objective_exponential(dataSet* dsptr);
int empirical_distribution(dataSet* dsptr);
double find_optimal_q(dataSet* dsptr);
int compute_density(dataSet* dsptr);
double compute_estimated_objective_atq(dataSet* dsptr, double q);
int plot_objective_function(dataSet* dsptr);









