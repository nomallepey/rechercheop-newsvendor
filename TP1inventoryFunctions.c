#include "TP1inventoryFunctions.h"
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/time.h>
#include<stdio.h>
#include<ilcplex/cplex.h>


int display_instance(dataSet* dsptr)
{
	int rval = 0;

	double p = dsptr->p;
	double c = dsptr->c;
	double s = dsptr->s;

	int K = dsptr->K;
	double*d_k = dsptr->d_k;

	fprintf(stderr,"\nWe have (p,c,s) = (%lf,%lf,%lf) and the following K=%d demand samples\n",
			p,c,s,K);
	fprintf(stderr,"--------------------------------------------------\n");


	for( int k = 0 ; k < K ; k++)
		fprintf(stderr,"%lf\t",d_k[k]);
	fprintf(stderr,"\n");

	fprintf(stderr,"--------------------------------------------------\n");

	return rval;
}



int read_newsvendor_samples(FILE*fin,dataSet* dsptr)
{
	int rval = 0;

	int K, seed;
	//Header
	fscanf(fin,"K;seed\n");
	fscanf(fin,"%d;%d\n",&K,&seed);

	dsptr->K = K;
	dsptr->seed = seed;

	dsptr->d_k = (double*)malloc(sizeof(double)*K);

	int k;
	for( k = 0 ; k < K ; k++)
		fscanf(fin,"%lf\n",&(dsptr->d_k[k]));

	fprintf(stderr,"\nInstance file read\n");

	return rval;
}

int free_IPproblem(IP_problem* ipprobptr)
{
	int rval = 0;

	//Internal structures
	rval = CPXfreeprob (ipprobptr->env, &(ipprobptr->lp));
	if(rval)
	{
		fprintf (stderr, "CPXfreeprob failed, error code %d.\n", rval);
		exit(0);
	}
	rval = CPXcloseCPLEX(&(ipprobptr->env));
	if(rval)
	{
		fprintf (stderr, "CPXcloseCPLEX failed, error code %d.\n", rval);
		exit(0);
	}
	//Output solution
	free(ipprobptr->x);
	//Costs array
	free(ipprobptr->cost);
	//TYpe of the variables (binary or continuous)
	free(ipprobptr->c_type);
	//Bounds over the variables
	free(ipprobptr->up_bound);
	free(ipprobptr->low_bound);
	//Names of the variables
	int nv = ipprobptr->nv;
	for(int v = 0 ; v < nv ; v++)
		free(ipprobptr->var_name[v]);
	free(ipprobptr->var_name);

	//Right hand section of the constraints
	free(ipprobptr->rhs);
	//Sense of the constraints
	free(ipprobptr->sense);
	//Left hand section for data constraints
	free(ipprobptr->rmatbeg);
	free(ipprobptr->rmatind);
	free(ipprobptr->rmatval);
	//Names of the constraints 
	int nc = ipprobptr->nc;
	for(int c = 0 ; c < nc ; c++)
		free(ipprobptr->const_name[c]);
	free(ipprobptr->const_name);



	return rval;
}


int plot_objective_exponential(dataSet* dsptr)
{
	int rval = 0;

	//XXX FILL HERE

	return rval;
}




//XXX USE THIS WITH qsort FOR THE EXTIMATED DISTRIBUTION
int doubleComparator (const void * a, const void * b)
{
	if (*(double*)a > *(double*)b)
		return 1;
	else if (*(double*)a < *(double*)b)
		return -1;
	else
		return 0;  
}

int empirical_distribution(dataSet* dsptr)
{
	int rval = 0;
	dsptr->support_size = dsptr->K;
	dsptr->values = (double*)malloc(sizeof(double)*dsptr->support_size);
	dsptr->density = (double*)malloc(sizeof(double)*dsptr->support_size);
	dsptr->acc = (double*)malloc(sizeof(double)*dsptr->support_size);
	memcpy(dsptr->values,dsptr->d_k,sizeof(double)*dsptr->support_size);
	qsort(dsptr->values, dsptr->support_size, sizeof(double),doubleComparator);

	for (int i = 0; i < dsptr->support_size; i++)
		dsptr->density[i] =  1.0/dsptr->support_size;

	dsptr->acc[0] = dsptr->density[0];
	for (int i = 1; i < dsptr->support_size; i++)
		dsptr->acc[i] = dsptr->acc[i-1] + dsptr->density[i];	
	/*
	// Display the arrays
	fprintf(stderr, "Values array: ");
	for (int i = 0; i < dsptr->support_size; i++)
		fprintf(stderr, "%lf ", dsptr->values[i]);
	fprintf(stderr, "\n");

	fprintf(stderr, "Density array: ");
	for (int i = 0; i < dsptr->support_size; i++)
		fprintf(stderr, "%lf ", dsptr->density[i]);
	fprintf(stderr, "\n");

	fprintf(stderr, "Accumulative array: ");
	for (int i = 0; i < dsptr->support_size; i++)
		fprintf(stderr, "%lf ", dsptr->acc[i]);
	fprintf(stderr, "\n"); */

	FILE*  accFile= fopen("accFile.csv", "w+");
	fprintf(accFile,"values (t); cumulée F(t);\n");
	for (int i = 0; i < dsptr->support_size; i++)
		fprintf(accFile, "%lf; %lf;\n ", dsptr->values[i], dsptr->acc[i]);


	return rval;
}



double find_optimal_q(dataSet* dsptr)
{
	double qstar = 0;

	FILE*  qFile= fopen("qs.csv", "a+");

	double raw_q = 1 - ( (dsptr->c + dsptr->s) / (dsptr->p + dsptr->s) );

	for(int i=0; i < dsptr->K - 1; i++)
	{
		if(dsptr->acc[i+1] > raw_q)
		{
			qstar = dsptr->values[i];

			break;
		}
	}

	dsptr->qstar = qstar;
	fprintf(stderr, "raw_q %lf\n",raw_q);
	fprintf(qFile, "%d; %lf;\n",dsptr->K, qstar);

	return qstar;
}

int compute_density(dataSet* dsptr)
{
	int rval = 0;

	//XXX FILL HERE

	return rval;
}

double compute_estimated_objective_atq(dataSet* dsptr, double q)
{
	double rval = 0;
	for (int i = 0; i < dsptr->support_size; i++) {

		double g = dsptr->density[i];
		double a = dsptr->values[i] * dsptr->p - dsptr->s * (q - dsptr->values[i]);

		if (a < dsptr->p * q) {
			g *= a;
		} else {
			g *= dsptr->p * q;
		}
		rval += g;
	}
	rval -= dsptr->c * q;

	return rval;
}


int plot_objective_function(dataSet* dsptr)
{
	int rval =0;
	FILE*  bFile= fopen("beneficesSelonQFile.csv", "w+");
	fprintf(bFile,"q; benefice;\n");
	for (double q = 0.0; q < 1; q += 0.01) {
		double obj = compute_estimated_objective_atq(dsptr, q);
		fprintf(bFile, "%lf; %lf;\n", q, obj);
	}
	return rval;
}


