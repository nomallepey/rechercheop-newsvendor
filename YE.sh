#!/bin/bash

# Loop example: Print numbers from 10 to 10000 by step of 50
for i in {10..10000..50}
do
    ./newsvendorGen -K$i

    ./TP1inventory -f newsvendorK${i}s2.csv

    rm newsvendorK${i}s2.csv
done